import os
import uproot
import awkward as ak
import pickle
import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import vector
vector.register_awkward()

save_collections = False
load_collections = False
do_cl3d_plots = True
do_efficiency_plot = True
do_resolution_plot = True

# Plotting details
output_dir = "./plots_full_loop"
if not os.path.isdir(output_dir):
    os.system("mkdir -p %s"%output_dir)

plotting_details = {
    "resolution":{
        "pt_bins" : np.linspace(20,100,5,dtype='int'),
        "nbins" : 20,
        "mean_yrange" : (-0.25,0.25),
        "std_yrange" : (0,0.3)
    },
    "efficiency_pt":{
        "bins" : np.linspace(20,100,9)
    },
    "efficiency_eta":{
        "bins" : np.linspace(1.5,3.0,11)
    },
    "cl3d_pt":{
        "nbins" : 40,
        "xrange" : [0,80],
        "logy" : True
    },
    "cl3d_bdteg":{
        "nbins" : 40,
        "xrange" : [-1,1],
        "logy" : True
    }

}

color_map = {
    "nominal":"deepskyblue",
    "standalone":"indianred"
}

variables = [
             'gen_pt','gen_eta','gen_phi','gen_energy', 
             'cl3d_pt', 'cl3d_energy', 'cl3d_eta', 'cl3d_phi', 'cl3d_clusters_n', 'cl3d_clusters_id', 'cl3d_showerlength', 'cl3d_coreshowerlength', 'cl3d_firstlayer', 'cl3d_maxlayer', 'cl3d_seetot', 'cl3d_seemax', 'cl3d_spptot', 'cl3d_sppmax', 'cl3d_szz', 'cl3d_srrtot', 'cl3d_srrmax', 'cl3d_srrmean', 'cl3d_emaxe', 'cl3d_hoe', 'cl3d_meanz', 'cl3d_layer10', 'cl3d_layer50', 'cl3d_layer90', 'cl3d_ntc67', 'cl3d_ntc90', 'cl3d_bdteg', 'cl3d_quality', 'cl3d_ipt', 'cl3d_ienergy'
            ]
variables_to_plot = ['cl3d_pt', 'cl3d_energy', 'cl3d_eta', 'cl3d_phi','cl3d_showerlength', 'cl3d_coreshowerlength', 'cl3d_firstlayer', 'cl3d_maxlayer', 'cl3d_seetot', 'cl3d_seemax', 'cl3d_spptot', 'cl3d_sppmax', 'cl3d_szz', 'cl3d_srrtot', 'cl3d_srrmax', 'cl3d_srrmean', 'cl3d_emaxe', 'cl3d_hoe', 'cl3d_meanz', 'cl3d_layer10', 'cl3d_layer50', 'cl3d_layer90', 'cl3d_ntc67', 'cl3d_ntc90', 'cl3d_bdteg', 'cl3d_quality']


# Uproot ntuple produced from HGCAL L1T TPG software
files = {}
files['nominal'] = "/vols/cms/jl2117/postdoc/upgrade/hgcal/emulator/Feb22/samples/DoublePhoton_FlatPt-1To100_PU200_nominal_full/ntuple_skimmed.root"
#files['standalone'] = "/vols/cms/jl2117/postdoc/upgrade/hgcal/emulator/Feb22/ntuple_newprocessors_skimmed_nevent200.root"

# Containers to store event information
events = {}
cl3d_selected = {}
gen_selected = {}
cl3d_genmatched = {}

from timeit import default_timer as timer
start_total = timer()

if load_collections:
    print(" --> Loading collections from analysis_loop.pkl")
    with open("analysis_loop.pkl", "rb") as fpkl: analysis = pickle.load(fpkl)
    events = analysis['events']
    gen_selected = analysis['gen_selected']
    cl3d_selected = analysis['cl3d_selected']
    cl3d_genmatched = analysis['cl3d_genmatched']

else:

    start = timer()

    for k,v in files.items():
        print(" --> Processing:", k)

        f = uproot.open(v)
        t = f["hgcalTriggerNtuplizer/HGCalTriggerNtuple"]

        # Print full set of tree branches
        print(" --> Input tree (%s) has variables:"%k, t.keys())

        # Extract all (required) event information into awkward array
        events[k] = t.arrays(variables, library='ak', how='zip')
        n_events = len(events[k])

        # Containers to store gen particles and clusters
        gen_selected[k] = []
        cl3d_selected[k] = []
        cl3d_genmatched[k] = []

        # Loop over events
        for i_event in range(n_events):
     
            gen_selected_event = []
            cl3d_selected_event = []
            cl3d_genmatched_event = []
            
            # Apply selection on gen level particles
            n_gen = len(events[k][i_event].gen)

            for i_gen in range(n_gen):

                gen_particle = events[k][i_event].gen[i_gen]
                if gen_particle.pt > 20: 
                    if( abs(gen_particle.eta) > 1.5 )&( abs(gen_particle.eta) < 3.0 ):
                        gen_selected_event.append(gen_particle)

            n_gen_selected = len(gen_selected_event)
                  
            # Apply selection to clusters
            n_cl3d = len(events[k][i_event].cl3d)

            for i_cl3d in range(n_cl3d):
     
                cl3d = events[k][i_event].cl3d[i_cl3d]
                if cl3d.pt > 10:
                    cl3d_selected_event.append(cl3d)

            n_cl3d_selected = len(cl3d_selected_event)

            # Perform gen-matching: loop over gen particles and find highest pt cluster with dR < 0.2
            for i_gen in range(n_gen_selected):
            
                gen_particle = gen_selected_event[i_gen]
                # Build 4-vector for gen particle
                gen_particle_p4 = vector.obj(pt=gen_particle.pt, phi=gen_particle.phi, eta=gen_particle.eta, E=gen_particle.energy)

                # Loop over selected clusters: select highest pt
                pt_max = 0
                cl3d_gm = None

                for i_cl3d in range(n_cl3d_selected):
                
                    cl3d = cl3d_selected_event[i_cl3d]
                    # Build 4-vector for cl3d
                    cl3d_p4 = vector.obj(pt=cl3d.pt, phi=cl3d.phi, eta=cl3d.eta, E=cl3d.energy)

                    # Calculate dR between gen particle and cluster
                    dR = gen_particle_p4.deltaR(cl3d_p4)

                    # If dR < 0.2 and pt > pt_max: fill cl3d_genmatched
                    if( dR < 0.2 )&( cl3d.pt > pt_max ):
                        cl3d_gm = cl3d
                        pt_max = cl3d.pt
                
                # Add gen particle/cl3d pair to event
                if cl3d_gm is not None:
                    cl3d_genmatched_event.append( {"gen":gen_particle, "cl3d":cl3d_gm} )

            # Save event info
            gen_selected[k].append( gen_selected_event )
            cl3d_selected[k].append( cl3d_selected_event )
            cl3d_genmatched[k].append( cl3d_genmatched_event )
                            
    end = timer()
    print(" --> Time elapsed processing events: %.3f s"%(end-start))

# Save collections
if save_collections:
    print(" --> Saving collections to analysis_loop.pkl")
    analysis = {
        "events" : events,
        "gen_selected" : gen_selected,
        "cl3d_selected" : cl3d_selected,
        "cl3d_genmatched": cl3d_genmatched
    }
    with open("analysis_loop.pkl", "wb") as fpkl: pickle.dump(analysis, fpkl)


# Plot cl3d_distributions
if do_cl3d_plots: 
 
    start = timer()

    for var in variables_to_plot:
        print(" --> Plotting: %s"%var)

        for k in files:

            x = []
            for event in cl3d_selected[k]:
                for cl3d in event:
                    x.append( cl3d["_".join(var.split("_")[1:])] )
            x = np.array(x)

            if var in plotting_details:
                x_range = plotting_details[var]["xrange"] if "xrange" in plotting_details[var] else None
                nbins = plotting_details[var]["nbins"] if "nbins" in plotting_details[var] else 50
            else:
                x_range = None
                nbins = 50

            if x_range is not None:
                n = plt.hist(x, nbins, range=(x_range[0],x_range[1]), facecolor=color_map[k], alpha=0.5, label=k)
            else:
                n = plt.hist(x, nbins, facecolor=color_map[k], alpha=0.5, label=k)

            # Save range of histogram so subsequent files have same binning
            if var not in plotting_details:
                plotting_details[var] = {}
                plotting_details[var]["xrange"] = [ n[1].min(), n[1].max() ] 
                plotting_details[var]["nbins"] = len(n[1])-1

        # Set plotting options
        if var in plotting_details:
            if "logy" in plotting_details[var]: plt.yscale("log")
            
        plt.xlabel(var)
        plt.ylabel('# of clusters')    
        plt.grid(True)
        plt.legend()

        plt.savefig("%s/%s.png"%(output_dir,var))
        plt.savefig("%s/%s.pdf"%(output_dir,var))

        plt.clf()

    end = timer()
    print(" --> Time elapsed plotting cluster properties: %.3f s"%(end-start))


# Plot efficiency curve in bins of gen pt, gen eta
if do_efficiency_plot:

    start = timer()

    for var in ['pt','eta']:
        print(" --> Plotting efficiency curve vs %s"%var)

        eff_genmatched, eff_genmatched_pass_egid = {}, {}

        for k in files:
            # Make three histograms: all gen, gen with matching cluster, gen with matching cluster passing egamma ID (quality==1)
            x = []
            for event in gen_selected[k]:
                for gen_particle in event:
                    x.append( gen_particle[var] )
            x = np.array(x)
            if var == "eta": x = abs(x)
            n_gen, bins = np.histogram(x, bins=plotting_details['efficiency_%s'%var]['bins'])

            x = []
            for event in cl3d_genmatched[k]:
                for pair in event:
                   x.append( pair['gen'][var] )
            x = np.array(x)
            if var == "eta": x = abs(x)
            n_genmatched, bins = np.histogram( x, bins=plotting_details['efficiency_%s'%var]['bins'])

            x = []
            for event in cl3d_genmatched[k]:
                for pair in event:
                    if pair['cl3d'].quality == 1:
                        x.append( pair['gen'][var] )
            x = np.array(x)
            if var == "eta": x = abs(x)
            n_genmatched_pass_egid, bins = np.histogram( x, bins=plotting_details['efficiency_%s'%var]['bins'])

            # Divide histograms to get efficiencies
            # TODO: add catch for zero bins
            eff_genmatched[k] = n_genmatched/n_gen
            eff_genmatched_pass_egid[k] = n_genmatched_pass_egid/n_gen
            # Add repeat value to end of array (step plotting in matplotlib)
            eff_genmatched[k] = np.append(eff_genmatched[k],eff_genmatched[k][-1])
            eff_genmatched_pass_egid[k] = np.append(eff_genmatched_pass_egid[k],eff_genmatched_pass_egid[k][-1])

            plt.step( plotting_details['efficiency_%s'%var]['bins'], eff_genmatched[k], color=color_map[k], label=k, where='post')
            plt.step( plotting_details['efficiency_%s'%var]['bins'], eff_genmatched_pass_egid[k], color=color_map[k], linestyle='dashed', label="%s (passing ID)"%k, where='post')

        plt.xlabel("Gen %s"%var)
        plt.ylabel("Efficiency")
        plt.grid()
        plt.legend()

        plt.savefig("%s/eff_vs_gen_%s.png"%(output_dir,var))
        plt.savefig("%s/eff_vs_gen_%s.pdf"%(output_dir,var))

        plt.clf()

    end = timer()
    print(" --> Time elapsed plotting efficiencies: %.3f s"%(end-start))

        

# Plot resolution distributions
if do_resolution_plot:

    start = timer()

    print(" --> Plotting cluster resolution")

    # Containers to store mean and std of resolution
    mean, std = {}, {}

    # Loop over pT ranges
    n_pt_bins = len(plotting_details['resolution']['pt_bins'])-1

    for i_pt_bin in range(n_pt_bins):

        pt_min = plotting_details['resolution']['pt_bins'][i_pt_bin]
        pt_max = plotting_details['resolution']['pt_bins'][i_pt_bin+1]
        pt_bin_str = "pt_%g_to_%g"%(pt_min,pt_max)
        mean[pt_bin_str] = {}
        std[pt_bin_str] = {}

        for k in files:

            # Extract gen matched clusters pt in pt range
            gen_pt, cl3d_pt = [], []
            for event in cl3d_genmatched[k]:
                for pair in event:
                    if( pair['gen'].pt >= pt_min )&( pair['gen'].pt < pt_max ):
                        gen_pt.append( pair['gen'].pt )
                        cl3d_pt.append( pair['cl3d'].pt )
            gen_pt = np.array( gen_pt )
            cl3d_pt = np.array( cl3d_pt )
            res = (cl3d_pt-gen_pt)/gen_pt

            plt.hist( res, bins=plotting_details['resolution']['nbins'], range=(-0.5,0.5), facecolor=color_map[k], alpha=0.5, label=k )

            # TODO: add gaussian fit to extract mean and std when enough events
            # Also add error on the values: plot with fill_between
            mean[pt_bin_str][k] = np.mean(res)
            std[pt_bin_str][k] = np.std(res)
       

        plt.xlabel("( pT(cl3d) - pT(gen) ) / pT(gen)")
        plt.ylabel("# of clusters")
        plt.grid()
        plt.legend()

        plt.savefig("%s/resolution_%s.png"%(output_dir,pt_bin_str))
        plt.savefig("%s/resolution_%s.pdf"%(output_dir,pt_bin_str))

        plt.clf()

    # Do summary plots: mean and std
    mean_steps = {}
    std_steps = {}
    for k in files:

        mean_steps[k] = []
        std_steps[k] = []

        for i_pt_bin in range(n_pt_bins):

            pt_min = plotting_details['resolution']['pt_bins'][i_pt_bin]
            pt_max = plotting_details['resolution']['pt_bins'][i_pt_bin+1]
            pt_bin_str = "pt_%g_to_%g"%(pt_min,pt_max)

            mean_steps[k].append( mean[pt_bin_str][k] )
            std_steps[k].append( std[pt_bin_str][k] )

        # Repeat final element, for matplotlib steps and convert to numpy array
        mean_steps[k].append( mean_steps[k][-1] )
        std_steps[k].append( std_steps[k][-1] )
        mean_steps[k] = np.array( mean_steps[k] )
        std_steps[k] = np.array( std_steps[k] )

    # Mean
    for k in files: plt.step( plotting_details['resolution']['pt_bins'], mean_steps[k], color=color_map[k], label=k, where='post')
    plt.xlabel("pT(gen)")
    plt.ylabel("Mean pT response")
    plt.ylim(plotting_details['resolution']['mean_yrange'])
    plt.grid()
    plt.legend()

    plt.savefig("%s/mean_pt_response.png"%(output_dir))
    plt.savefig("%s/mean_pt_response.pdf"%(output_dir))

    plt.clf()

    # Std
    for k in files: plt.step( plotting_details['resolution']['pt_bins'], std_steps[k], color=color_map[k], label=k, where='post')
    plt.xlabel("pT(gen)")
    plt.ylabel("STD pT response")
    plt.ylim(plotting_details['resolution']['std_yrange'])
    plt.grid()
    plt.legend()

    plt.savefig("%s/std_pt_response.png"%(output_dir))
    plt.savefig("%s/std_pt_response.pdf"%(output_dir))

    plt.clf()

    end = timer()
    print(" --> Time elapsed plotting resolution: %.3f s"%(end-start))

print("*******************************************************")
end_total = timer()
print(" --> Total time elapsed: %.3f s"%(end_total-start_total))


