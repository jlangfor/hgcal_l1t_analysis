import os
import uproot
import awkward as ak
import pickle
import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import vector
vector.register_awkward()

from optparse import OptionParser

def get_options():
  parser = OptionParser()
  parser.add_option("-i","--input-ntuple", dest="input_ntuple", default='simulation:/vols/cms/jl2117/postdoc/upgrade/hgcal/emulator/Feb22/samples/DoublePhoton_FlatPt-1To100_NoPU_simulation_pass2/ntuple_simulation.root', help="Input ntuple")
  parser.add_option("-o","--output-dir", dest="output_dir", default='efficiency_maps', help="Output dir to store plots")
  parser.add_option("-l","--label-str", dest="label_str", default='CMSSW simulation (0 PU)', help="Label to add to plot")
  parser.add_option("-s","--save", dest="save", default='efficiency_map_test', help="Plot name")
  return parser.parse_args()
(opt,args) = get_options()

# Uproot ntuple produced from HGCAL L1T TPG software
files = {}
files[opt.input_ntuple.split(":")[0]] = opt.input_ntuple.split(":")[1]


# Plotting details
output_dir = opt.output_dir
if not os.path.isdir(output_dir):
    os.system("mkdir -p %s"%output_dir)

plotting_details = {
    "efficiency_map":{
        "ROverZBins" : np.linspace(0.075,0.56,12),
        "PhiBins" : np.linspace(-1*np.pi/9,7*np.pi/9,17)
    }
}

variables = {
    "simulation":{
        "gen":["pt","eta","phi","energy"],
        "cl3d":['pt', 'energy', 'eta', 'phi',"id"]
    },
    "emulator":{
        "gen":["pt","eta","phi","energy"],
        "cl3dSA":["pt","eta","phi","energy"]
    }
}

cluster_key = {
    "simulation":"cl3d",
    "emulator":"cl3dSA"
}

# Containers to store event information
events = {}
cl3d_selected = {}
gen_selected = {}
cl3d_genmatched = {}
cl3d_nongenmatched = {}

def plot_numbers(ax,X,Y,a,err_a=None):
    nx, ny = np.shape(a)
    for ix in range(nx):
        x = 0.5*(X[0][ix]+X[0][ix+1])
        for iy in range(ny):
            y = 0.5*(Y[iy][0]+Y[iy+1][0])
            y_width = abs(Y[iy][0]-Y[iy+1][0])
            z = a[ix][iy]
            # Skip nans
            if z == z:
                if err_a is not None:
                    #ax.text(x,y,"%.2f+-%.2f"%(z,err_a[ix][iy]),fontdict={'size':4},va='center',ha='center')
                    ax.text(x,y+0.15*y_width,"%.3f"%z,fontdict={'size':6},va='center',ha='center')
                    ax.text(x,y-0.2*y_width,"(+/-%.3f)"%err_a[ix][iy],fontdict={'size':4},va='center',ha='center')

from timeit import default_timer as timer
start_total = timer()

start = timer()

for k,v in files.items():
    print(" --> Processing:", k)

    f = uproot.open(v)
    t = f["hgcalTriggerNtuplizer/HGCalTriggerNtuple"]

    # Print full set of tree branches
    print(" --> Input tree (%s) has variables:"%k, t.keys())

    # Extract all (required) event information into awkward array
    vars_to_store = []
    for var_type in variables[k.split("_")[0]]:
      for var in variables[k.split("_")[0]][var_type]:
          vars_to_store.append("%s_%s"%(var_type,var))
    vars_to_store.extend(['run','event','lumi'])
    events[k] = t.arrays(vars_to_store, library='ak', how='zip')

    # Extract cluster and gen particles in event as four vectors
    cl3d = ak.Array( events[k][cluster_key[k.split("_")[0]]], with_name="Momentum4D")
    gen = ak.Array( events[k]['gen'], with_name="Momentum4D")

    # Gen selection: pT > 2 GeV, 1.5 < |eta| < 3.0
    gen = gen[gen.pt>2]
    #gen = gen[gen.pt>2]
    gen = gen[(abs(gen.eta)>1.5)&(abs(gen.eta)<3.0)]
    # Save selected gen particles
    gen_selected[k] = gen

    # Cluster selection: pT > 2 GeV
    cl3d = cl3d[cl3d.pt>2]
    #cl3d = cl3d[cl3d.pt>2]

    # Give clusters unique cluster id
    cl3d_unique_id = []
    previous_id = 0
    for i in ak.to_list(ak.num(cl3d)):
        cl3d_unique_id.append( np.linspace(previous_id,previous_id+i-1,i, dtype='int') )
        previous_id += i
    cl3d['unique_id'] = ak.Array(cl3d_unique_id)

    # Save selected clusters
    cl3d_selected[k] = cl3d 

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Gen matching:
    # Make all combinations of clusters and gen particles
    gen_cl3d_pairs = ak.cartesian( {"gen":gen, "cl3d":cl3d} )
    # Extract args of gen and cl3d in pairs
    gen_cl3d_pairs_arg = ak.argcartesian( {"gen":gen, "cl3d":cl3d} )

    # Consider only clusters which have dR < 0.2 with gen particle
    dR_mask = gen_cl3d_pairs.gen.deltaR( gen_cl3d_pairs.cl3d ) < 0.2
    gen_cl3d_pairs_genmatched = gen_cl3d_pairs[dR_mask]
    gen_cl3d_pairs_arg_genmatched = gen_cl3d_pairs_arg[dR_mask]

    # If multiple clusters for gen particle, choose cl3d with highest pt
    # First order clusters by pt
    order_cl3d = ak.argsort(gen_cl3d_pairs_genmatched.cl3d.pt, ascending=False, axis=1)
    gen_cl3d_pairs_genmatched = gen_cl3d_pairs_genmatched[order_cl3d]
    gen_cl3d_pairs_arg_genmatched = gen_cl3d_pairs_arg_genmatched[order_cl3d]

    # Add event idx identifier to gen_cl3d pairs
    gen_cl3d_pairs_genmatched['ev_idx'] = np.linspace(0,len(gen_cl3d_pairs_genmatched)-1,len(gen_cl3d_pairs_genmatched), dtype='int')

    # Skim up to max number of gen particles
    n_gen_max = ak.max( ak.num(gen) )
    arr = []
    for i_gen in range( n_gen_max ):
      # Select clusters with same gen particle id
      mask = (gen_cl3d_pairs_arg_genmatched.gen == i_gen)
      # Find first cluster (highest pt) and append array to arr
      firsts = ak.firsts( gen_cl3d_pairs_genmatched[mask], axis=1 )
      # Remove nones
      firsts = firsts[~ak.is_none(firsts)]
      arr.append( firsts )

    # Concatenate array of gen matched clusters, order by event id and save
    cl3d_genmatched[k] = ak.concatenate(arr)
    cl3d_genmatched[k] = cl3d_genmatched[k][ ak.argsort( cl3d_genmatched[k].ev_idx, ascending=True ) ]
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    
end = timer()
print(" --> Time elapsed processing events: %.3f s"%(end-start))


# Simple phi-mapping to 120 sector: replace with more sophisticated cluster-to-FPGA mapping (if possible)
mask_sector0 = (cl3d_genmatched[k].gen.phi<-np.pi/3)
mask_sector1 = (cl3d_genmatched[k].gen.phi>=-np.pi/3)&(cl3d_genmatched[k].gen.phi<np.pi/3)
mask_sector2 = (cl3d_genmatched[k].gen.phi>=np.pi/3)

eta_sector0, phi_sector0 = cl3d_genmatched[k].gen[mask_sector0].eta, cl3d_genmatched[k].gen[mask_sector0].phi+np.pi
roverz_sector0 = abs(2*np.exp(-1*eta_sector0)/(1-np.exp(-2*eta_sector0)))

eta_sector1, phi_sector1 = cl3d_genmatched[k].gen[mask_sector1].eta, cl3d_genmatched[k].gen[mask_sector1].phi+(np.pi/3)
roverz_sector1 = abs(2*np.exp(-1*eta_sector1)/(1-np.exp(-2*eta_sector1)))

eta_sector2, phi_sector2 = cl3d_genmatched[k].gen[mask_sector2].eta, cl3d_genmatched[k].gen[mask_sector2].phi-(np.pi/3)
roverz_sector2 = abs(2*np.exp(-1*eta_sector2)/(1-np.exp(-2*eta_sector2)))

roverz = np.concatenate([roverz_sector0,roverz_sector1,roverz_sector2])
phi = np.concatenate([phi_sector0,phi_sector1,phi_sector2])

hist_gm = np.histogram2d(roverz,phi,bins=(plotting_details['efficiency_map']['ROverZBins'],plotting_details['efficiency_map']['PhiBins']))[0]

# All gen particles
gp = ak.flatten(gen_selected[k])
mask_sector0 = gp.phi<-np.pi/3
mask_sector1 = (gp.phi>=-np.pi/3)&(gp.phi<np.pi/3)
mask_sector2 = gp.phi>=np.pi/3

eta_sector0, phi_sector0 = gp[mask_sector0].eta, gp[mask_sector0].phi+np.pi
roverz_sector0 = abs(2*np.exp(-1*eta_sector0)/(1-np.exp(-2*eta_sector0)))

eta_sector1, phi_sector1 = gp[mask_sector1].eta, gp[mask_sector1].phi+(np.pi/3)
roverz_sector1 = abs(2*np.exp(-1*eta_sector1)/(1-np.exp(-2*eta_sector1)))

eta_sector2, phi_sector2 = gp[mask_sector2].eta, gp[mask_sector2].phi-(np.pi/3)
roverz_sector2 = abs(2*np.exp(-1*eta_sector2)/(1-np.exp(-2*eta_sector2)))

roverz = np.concatenate([roverz_sector0,roverz_sector1,roverz_sector2])
phi = np.concatenate([phi_sector0,phi_sector1,phi_sector2])

hist_total, xedges, yedges = np.histogram2d(roverz,phi,bins=(plotting_details['efficiency_map']['ROverZBins'],plotting_details['efficiency_map']['PhiBins']))

eff = hist_gm/hist_total
#eff = np.nan_to_num(eff)
X,Y = np.meshgrid(xedges, yedges)

# Calculate Bayesian variance on the efficiency: sqrt to get error
# https://indico.cern.ch/event/66256/contributions/2071577/attachments/1017176/1447814/EfficiencyErrors.pdf
sigma_eff = np.sqrt(((hist_gm+1)*(hist_gm+2))/((hist_total+2)*(hist_total+3))-((hist_gm+1)**2/(hist_total+2)**2)) 

plt.set_cmap("RdYlGn")
fig = plt.figure()
ax = plt.gca()
mat = ax.pcolormesh(X,Y,eff.T,vmin=0.8,vmax=1.0) #,cmap=plt.get_cmap("RdYlGn"))
plot_numbers(ax,X,Y,eff,sigma_eff)

ax.set_xlabel("r/z")
ax.set_ylabel("$\\phi$")
ax.set_xlim(0.05,0.5)
ax.set_ylim(-0.1,2.2)

fig.text(0.12,0.92,opt.label_str,fontsize=12)

cbar = fig.colorbar(mat)
cbar.set_label("Efficiency")

fig.savefig("%s/%s.png"%(output_dir,opt.save))
fig.savefig("%s/%s.pdf"%(output_dir,opt.save))

fig.clf()
   

    

print("*******************************************************")
end_total = timer()
print(" --> Total time elapsed: %.3f s"%(end_total-start_total))
